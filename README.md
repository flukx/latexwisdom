# LaTeX Wisdom

*für Deutsch s. unten*

In this document I have collected some experiences with LaTeX.
For further idea behind it, read the result.

You can read the [document in English](https://gitlab.com/api/v4/projects/10234469/jobs/artifacts/master/raw/latexwisdom-en.pdf?job=pdf)

Please note that some sections are only understandable if you read the source code.

## Deutsch

In diesem Dokument schreibe ich auf, was ich so über die LaTeX Nutzung gelernt habe.

Ein paar Teile sind ins Deutsche übersetzt.
Diese in deutsch und den Rest in Englisch findest du [hier](https://gitlab.com/api/v4/projects/10234469/jobs/artifacts/master/raw/latexwisdom-de.pdf?job=pdf)
Bitte beachte, dass einige Abschnitte nur verständlich sind, wenn man den Quellcode liest.


