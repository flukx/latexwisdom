%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary
\docStart
\section{Math mode}
\subsection{Inline Math mode}%
\label{sec:inline_math_mode}

Inline math mode is enabled with \lstinline|$|.
It is also possible to use \lstinline|\(formula\)| and I have seen debates on what is better.
Use what you like.

It is important to use the inline math mode even
when \LaTeX{} does not give an error without: for a single
variable $A$ just writing A is a single letter
while with \lstinline|$| ($A$) gives a mathematical variable.
The meaning is totally different and that is reflected
in the print. The same is true for numbers like $123$.
While in the standard font it may seem irrelevant, in other
fonts it is not and we want semantically correct code anyway.

Limits as in $Σ_{i = 1}^n \sup_{x ∈ M} \abs x$ can be put
underneath/ on top of the operators 
(\enquote{display-math style}) but I think that is a
bad idea. Putting the limits beside the operator makes
the formula fit a bit better into the line. If you want 
display-math style equations, then they are
probably big enough to be put into actual display-math
environments.

\subsection{Display Math mode}%
\label{sec:display_math_mode}

Display math mode like
\begin{equation*}
  Σ_{i = 1}^n \sup_{x ∈ M} \abs x
\end{equation*}
is used for actual formulas, equations and the like.
Some heuristics if you should use display math environments
might include
\begin{itemize}
  \item You want to use display math style in inline math,
    use display math environment instead (see \ref{sec:inline_math_mode}).
  \item The formula is over half a line long.
  \item The formula involves a line break.
  \item The line of the formula is considerable higher
    than other lines, e.\,g.\ due to fractions with exponents.
\end{itemize}

There are different ways to enter display math environments
for different use cases. The main points are written at the
beginning of the chapter 3 Display equations of the documentation of the package
\lstinline|amsmath|\footnote{\lstinline|texdoc amsmath| or
\url{http://texdoc.net/texmf-dist/doc/latex/amsmath/amsldoc.pdf}}.
A lot of details are following there, including tagging,
complexly structured math environments, \dots.
It is very good!
\begin{itemize}
  \item \lstinline|$$| has no use case whatsoever. Do not use it!
    It is a \TeX{} internal and a \enquote{Deadly sin}.
    See \url{https://ftp.gwdg.de/pub/ctan/info/l2tabu/english/l2tabuen.pdf}
    section 1.6 for details.
  \item \lstinline|\[...\]| may be preverable over \lstinline|$$|
    but still not good and has no use case. For \LaTeX{} it is
    equivalent to the environment \lstinline|equation*| as soon
    as you load the package \lstinline|amsmath|. But for the
    author
    \begin{enumerate}
      \item it is less readable because you cannot guess the
        meaning if you are not familiar with it (as me).
        You use environments for many other things like
        float-objects, theorems, proofs, other math
        environments like \lstinline|align|\dots.
        Consistency calls for using \lstinline|equation*|.
      \item it changes its appearance in the output \lstinline|pdf|
        as soon as you load \lstinline|amsmath|
        for some other reason.
      \item your code becomes inconsistent as soon as you
        use the \lstinline|equation|-environment (without \lstinline|*|!)
    \end{enumerate}
  \item \lstinline|equation(*)|, possibly with
     \lstinline|split| and \lstinline|align(*)| are to be used,
     well explained in the \lstinline|amsmath|-package documentation
   \item \lstinline|eqnarray| is deprecated (see \lstinline|amsmath|-doc).
     Do not use it.
   \item \lstinline|displaymath|-environment is from the
     \LaTeX-view equivalent to \lstinline|equation*| but
     is inconsistent with \lstinline|equation|-environment. Do
     not use it.
\end{itemize}
%
\subsection{Alignment}%
\label{sec:alignment}

In aligned math environments (most prominently \lstinline|align(*)|,
\lstinline|alignedat| and \lstinline|split| we can make sure that
specific things are arranged under each other. This is called alignment
and very common for relation symbols like $=$:
\begin{align*}
  \abs v &= \abs{v - w + w} \\
         &\leq \abs{v - w} + \abs{w}
\end{align*}
For understanding what is happening here it helps to know that
\lstinline|align(*)| is just a table with alternating right- and
left-aligned columns. The spacing is set in such a way that the
relation symbol must come after the \lstinline|&|, i.\,e.\ at the
beginning of a cell.

Sometimes we want to justify some step in the calculation.
There are different ways to do that:
\TODO{side-by-side code}
\begin{align}
  \abs v &= \abs{v - w + w} \notag \\
         &\leq \abs{v - w} + \abs{w} && \text{(triangle inequality)} \label{eq:behind}
\end{align}
\begin{align}
  \abs v &= \abs{v - w + w} \notag \\
         &\leq \abs{v - w} + \abs{w} \qquad \text{(triangle inequality)} \label{eq:behindspace}
\end{align}
\begin{align}
  \abs v &= \abs{v - w + w} \notag \\
         &\stackrel{\Delta}\leq \abs{v - w} + \abs{w} \label{eq:above}
\end{align}
\begin{align}
  \abs v &= \abs{v - w + w} \notag \\
  \overset{\text{triangle inequality}}&\leq \abs{v - w} + \abs{w} \label{eq:longabove}
\end{align}
\begin{align}
  \abs v &= \abs{v - w + w} \notag \\
  \intertext{and by the triangle inequality}
  &\leq \abs{v - w} + \abs{w} \label{eq:intertext}
\end{align}
First of all, consistency is important. Changing from one
way to another from line to line annoys any reader. Since some
comments might be much longer than others, different ways to
arrange them can be fine too, though. Ask your common sense and
the person next to you.

The version \ref{eq:behind} is my favorite as long as there is
enough space. Especially for long formulas and explanations a page
is too narrow though. Version \ref{eq:behindspace} can sometimes
be shorter and fit into a line where \ref{eq:behind} is too long
but if you have several explanations in several lines you loose
the alignment.

For longer texts that to not fit on a line together with
formulas \lstinline|amsmath| provides \lstinline|\intertext|
that works as you would have to math environments but with
same alignment. Look at the code and the
\lstinline|amsmath|-documentation for usage.

For one-symbol explanations version \ref{eq:above} with
\lstinline|\stackrel{}{}| is useful. \lstinline|stackrel| can also be used
without alignment. The problem that easily arises here is that
the relation symbol together with the text above is one object
at the left end of the column. With texts above the relation symbol
which are longer that the relation symbol, the relation moves
right which looks ugly. Try for yourself!
The solution is provided by the package \lstinline|aligned-overset|
with a generalisation of \lstinline|\stackrel|: \lstinline|\overset|.
Look at the code for an example.
Since in aligned environments \lstinline|\overset| can always be used
and \lstinline|\stackrel| only for one-symbol explanations \lstinline|\overset|
together with \lstinline|aligned-overset| is to be preferred.

\subsection{Find the right command}
If you do not know what a makro is called, use the website
\url{http://detexify.kirelabs.org/classify.html}.
Also consider using unicode, see section \ref{sec:unicode}.
\docEnd
